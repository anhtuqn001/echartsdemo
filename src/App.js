import React, { Component, } from 'react';
import './App.css';
import io from 'socket.io-client';
import ReactEcharts from 'echarts-for-react';

var socket = io('http://45.117.169.208:881', {
  path: '/api'  
});



var dataForChart = {
  _1second: {},
  _1minute: {},
  _3minutes: {},
  _5minutes: {},
  _30minutes: {},
  _1hour: {},
  _6hours: {},
  _1day: {}
}
for (var key in dataForChart) {
  dataForChart[key] = {
    timelines: [],
    buyValues: [],
    sellValues: [],
  }
  for (let i = 0; i < 60; i++) {
    dataForChart[key].timelines[i] = 0;
    dataForChart[key].buyValues[i] = 0;
    dataForChart[key].sellValues[i] = 0;
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataForChart : dataForChart,
      timeType : "_1second"
    }
    this.initializeCurrentValueData = this.initializeCurrentValueData.bind(this);
    this.updateChart = this.updateChart.bind(this);
    this.setDataState = this.setDataState.bind(this);
    this.updateChartWithSecondData = this.updateChartWithSecondData.bind(this);
    this.handleAxisData = this.handleAxisData.bind(this);
    this.on1SecondButtonClick = this.on1SecondButtonClick.bind(this);
    this.on1MinuteButtonClick = this.on1MinuteButtonClick.bind(this);
    this.on3MinutesButtonClick = this.on3MinutesButtonClick.bind(this);
    this.on5MinutesButtonClick = this.on5MinutesButtonClick.bind(this);
    this.on30MinutesButtonClick = this.on30MinutesButtonClick.bind(this);
    this.on1HourButtonClick = this.on1HourButtonClick.bind(this);
    this.on6HoursButtonClick = this.on6HoursButtonClick.bind(this);
    this.on1DayButtonClick = this.on1DayButtonClick.bind(this);
  }

  componentWillMount() {
    this.initializeCurrentValueData();
  }

  initializeCurrentValueData() {
    this.currentValueData = {
      _1minute: { buyValue: 0, sellValue: 0 },
      _3minutes: { buyValue: 0, sellValue: 0 },
      _5minutes: { buyValue: 0, sellValue: 0 },
      _30minutes: { buyValue: 0, sellValue: 0 },
      _1hour: { buyValue: 0, sellValue: 0 },
      _6hours: { buyValue: 0, sellValue: 0 },
      _1day: { buyValue: 0, sellValue: 0 }
    }
  }

  updateChart(data) {
    var { timer, value } = data;
    switch (timer.toString()) {
      case "60":
        this.setDataState(value, "_1minute");
        break;
      case "180":
        this.setDataState(value, "_3minutes");
        break;
      case "300":
        this.setDataState(data, "_5minutes");
        break;
      case "1800":
        this.setDataState(data, "_30minutes");
        break;
      case "3600":
        this.setDataState(data, "_1hour");
        break;
      case "21600":
        this.setDataState(data, "_6hours");
        break;
      case "86400":
        this.setDataState(data, "_1day");
        break;
    }
  }

  setDataState(value, timeType) {
    var roundedBuyValue = Math.round(value.buy * 10) / 10;
    var roundedSellValue = Math.round(value.sell * 10) / 10;
    var currentValueData = this.currentValueData;
    var dataForChart = this.state.dataForChart;
    var newBuyValues = dataForChart[timeType].buyValues.slice(0);
    var newSellValues = dataForChart[timeType].sellValues.slice(0);
    var newTimelines = dataForChart[timeType].timelines.slice(0)
    if (roundedBuyValue < currentValueData[timeType].buyValue) {
      var now = new Date();
      newBuyValues.shift();
      newBuyValues.push(currentValueData[timeType].buyValue);
      newSellValues.shift();
      newSellValues.push(currentValueData[timeType].sellValue);
      newTimelines.shift();
      newTimelines.push(now);
      dataForChart[timeType] = { timelines : newTimelines, sellValues: newSellValues , buyValues: newBuyValues }
      this.setState({
        dataForChart: { ...dataForChart, [timeType]: dataForChart[timeType] }
      });
    }
    this.currentValueData[timeType] = { buyValue: roundedBuyValue, sellValue: roundedSellValue }
  }

  handleAxisData(timelines, timeType) {
    switch (timeType) {
      case "_1second" :  
      var newTimelines = timelines.map(item => item == 0 ? item : item.getSeconds());
      return newTimelines;
      case (timeType.match(/minute/) ? timeType : undefined) :
      var newTimelines = timelines.map(item => item == 0 ? item : item.getMinutes());
      return newTimelines;
      case (timeType.match(/hour/) ? timeType : undefined) :
      var newTimelines = timelines.map(item => item == 0 ? item : item.getHours());
      return newTimelines; 
      case "_1day" :  
      var newTimelines = timelines.map(item => item == 0 ? item : item.getDate());
      return newTimelines;
    }
  }
  getOption = () => {
    var { timelines, buyValues, sellValues } = this.state.dataForChart[this.state.timeType];
    return {
      title: {
        text: 'ECharts entry example'
      },
      tooltip: {},
      grid : [
        {
          left: '4%',
          right: '4%',
          top: '55%',
          height: '40%'
        },
        {
          left: '4%',
          right: '4%',
          height: '40%'
        }
      ],
      legend: {
        data: ['BuyVolume','SellVolume']
      },
      xAxis: [
      {
        type: 'category',
        data: this.handleAxisData(timelines, this.state.timeType)
      },
      {
        type: 'category',
        data: this.handleAxisData(timelines, this.state.timeType),
        gridIndex: 1,
        position : 'top'
      }
      ],
      yAxis: [
      {},
      {
        gridIndex: 1,
        inverse : true
      }
        ],
      series: [
      {
        name: 'BuyVolume',
        type: 'bar',
        data: buyValues
      },
      {
        name: 'SellVolume',
        type: 'bar',
        xAxisIndex: 1,
        yAxisIndex: 1,
        data: sellValues
      },
    ]
    }
  }

  updateChartWithSecondData(data) {
    var roundedSellValue = Math.round(data[0] * 10) / 10;
    var roundedBuyValue = Math.round(data[2] * 10) / 10;
    var now = new Date;
    var dataForChart = this.state.dataForChart;
    var newSellValues = dataForChart["_1second"].sellValues.slice(0);
    var newBuyValues = dataForChart["_1second"].buyValues.slice(0);
    var newTimelines = dataForChart["_1second"].timelines.slice(0);
    newSellValues.shift();
    newSellValues.push(roundedSellValue);
    newBuyValues.shift();
    newBuyValues.push(roundedBuyValue);
    newTimelines.shift();
    newTimelines.push(now);
    dataForChart["_1second"] = {timelines : newTimelines, buyValues : newBuyValues, sellValues : newSellValues}
    this.setState({
      dataForChart : {...dataForChart, ["_1second"] : dataForChart["_1second"] }
    })
  }

  componentDidMount() {
    socket.open();
    let thiz = this;
    socket.on('trade data', (data) => {
      thiz.updateChartWithSecondData(data);
    });
    socket.on('trade record', (data) => {
      thiz.updateChart(data);
    });
  }

  componentDidUpdate() {
    console.log("changed");
  }

  on1SecondButtonClick() {
    this.setState({
      timeType : "_1second"
    });
  }

  on1MinuteButtonClick() {
    this.setState({
      timeType : "_1minute"
    });
  }

  on3MinutesButtonClick() {
    this.setState({
      timeType : "_3minutes"
    });
  }

  on5MinutesButtonClick() {
    this.setState({
      timeType : "_5minutes"
    });
  }

  on30MinutesButtonClick() {
    this.setState({
      timeType : "_30minutes"
    });
  }

  on1HourButtonClick() {
    this.setState({
      timeType : "_1hour"
    });
  }

  on6HoursButtonClick() {
    this.setState({
      timeType : "_6hours"
    });
  }

  on1DayButtonClick() {
    this.setState({
      timeType : "_1day"
    });
  } 

  render() {
    return (
      <div>
        <ReactEcharts option={this.getOption()} style={{ height: '600px', width: '100%' }}
          className='react_for_echarts' />
        <div style={{ margin : 'auto', width : '40%', display: 'flex', justifyContent: 'space-around'}}>
        <button onClick={this.on1SecondButtonClick}>1 Second</button>
        <button onClick={this.on1MinuteButtonClick}>1 Minute</button>
        <button onClick={this.on3MinutesButtonClick}>3 Minutes</button>
        <button onClick={this.on5MinutesButtonClick}>5 Minutes</button>
        <button onClick={this.on30MinutesButtonClick}>30 Minutes</button>
        <button onClick={this.on1HourButtonClick}>1 Hour</button>
        <button onClick={this.on6HoursButtonClick}>6 Hours</button>
        <button onClick={this.on1DayButtonClick}>1 Day</button>
        </div>
      </div>
    );
  }
}

export default App;
